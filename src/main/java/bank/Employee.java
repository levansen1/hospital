/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bank;


public class Employee {
    private String name;
    
    Employee(String name){
        this.name = name;
    }
    
    public String getEmployeeName(){
        return this.name;
    }
}

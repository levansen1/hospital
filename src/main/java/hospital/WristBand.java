/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

import java.util.ArrayList;

public class WristBand {
    private String barCode;
    private ArrayList<Patient> patients = new ArrayList<Patient>();

    public WristBand(String barCode, ArrayList<Patient> patients) {
        this.barCode = barCode;
        this.patients = patients;
        
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }
    
}

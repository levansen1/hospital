/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package institute;

import java.util.ArrayList;

public class Department {
   private String name;
   private ArrayList<Student> students;

    public Department(String name, ArrayList<Student> students) {
        this.name = name;
        this.students = students;
    }
   public ArrayList<Student> getStudents(){
       return students;
   }
   
   
}
